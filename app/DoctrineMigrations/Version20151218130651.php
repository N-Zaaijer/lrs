<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151218130651 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE safe_q_user (id INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT \'Unique identifier of SafeQ User\', username LONGTEXT NOT NULL COMMENT \'Textual identifier for SafeQ user\', email LONGTEXT NOT NULL COMMENT \'Email address of SafeQ user\', first_name LONGTEXT NOT NULL COMMENT \'First name of SafeQ user\', last_name LONGTEXT NOT NULL COMMENT \'last name of SafeQ user\', rfid LONGTEXT NOT NULL COMMENT \'RFID of SafeQ user\', last_scan_date DATETIME NOT NULL COMMENT \'Date RFID was last scanned\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'Table with SafeQ users\' ');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'agnes.gerrits\',\'a.gerrits@drukwerkdeal.nl\',\'Agnes\',\'Gerrits\',\'80366612474F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'alfred.vanderveen\',\'a.vanderveen@drukwerkdeal.nl\',\'Alfred\',\' van der Veen\',\'803A85D26B8C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'anja.koetsier\',\'a.koetsier@drukwerkdeal.nl\',\'Anja\',\'Koetsier\',\'803A85D26E9704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'annemiek.vandeheg\',\'annemiek.vandeheg@drukwerkdeal.nl\',\'Annemiek\',\'van de Heg\',\'80366612482D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'bart.klunder\',\'bart.klunder@drukwerkdeal.nl\',\'Bart\',\'Klunder\',\'80366612416604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'bart.veneman\',\'bart.veneman@drukwerkdeal.nl\',\'Bart\',\'Veneman\',\'803A85D2796804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'bart.wekking\',\'bart.wekking@drukwerkdeal.nl\',\'Bart\',\'Wekking\',\'803A85D2737404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'bjorn.scholten\',\'b.scholten@drukwerkdeal.nl\',\'Bjorn\',\'Scholten\',\'803A85D2571F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'byrge.leeuwangh\',\'byrge.leeuwangh@drukwerkdeal.nl\',\'Byrge\',\'Leeuwangh\',\'803A85D2621C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'chiel.slinkman\',\'chiel.slinkman@drukwerkdeal.nl\',\'Chiel\',\'Slinkman\',\'803A85D2487C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'chris.keizers\',\'chris.keizers@drukwerkdeal.nl\',\'Chris\',\'Keizers\',\'803A85D2428504\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'coen.doldersum\',\'c.doldersum@drukwerkdeal.nl\',\'Coen\',\'Doldersum\',\'803A85D26C2104\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'colette.koffeman\',\'c.koffeman@drukwerkdeal.nl\',\'Colette\',\'Koffeman\',\'803A85D24E9404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'daniel.floor\',\'daniel.floor@drukwerkdeal.nl\',\'Daniel\',\'Floor\',\'803A85D24B8B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'danny.modderman\',\'\',\'Danny\',\'Modderman\',\'80366612478804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dave.corporaal\',\'dave.corporaal@drukwerkdeal.nl\',\'Dave\',\'Corporaal\',\'80366612484B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'davey.stokkers\',\'davey.stokkers@drukwerkdeal.nl\',\'Davey\',\'Stokkers\',\'803A85D2486B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dennika.gerard\',\'dennika.gerard@drukwerkdeal.nl\',\'Dennika\',\'de Jong\',\'80366612428404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dennis.wessels\',\'dennis.wessels@drukwerkdeal.nl\',\'Dennis\',\'Wessels\',\'803A85D2442A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dick.vloedgraven\',\'dick.vloedgraven@drukwerkdeal.nl\',\'Dick\',\'Vloedgraven\',\'803A85D2423B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dinant.hendriksen\',\'dinant.hendriksen@drukwerkdeal.nl\',\'Dinant\',\'Hendriksen\',\'803A85D2587904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dinette.nijkamp\',\'d.nijkamp@drukwerkdeal.nl\',\'Dinette\',\'Nijkamp\',\'803A85D24D8904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'egwin.robaard\',\'egwin.robaard@drukwerkdeal.nl\',\'Egwin\',\'Robaard\',\'803A85D2786704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'elbert.wobben\',\'elbert.wobben@drukwerkdeal.nl\',\'Elbert\',\'Wobben\',\'80366612437D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ellen.brinkman\',\'ellen.brinkman@drukwerkdeal.nl\',\'Ellen\',\'Brinkman\',\'803A85D2673A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ellen.houtman\',\'ellen.houtman@drukwerkdeal.nl\',\'Ellen\',\'Houtman\',\'803A85D26E2F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'elvira.hughan\',\'elvira.hughan@drukwerkdeal.nl\',\'Elvira\',\'Hughan\',\'803665FA2C9804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'eric.veijer\',\'eric.veijer@drukwerkdeal.nl\',\'Eric\',\'Veijer\',\'803A85D25C2A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'eric.weder\',\'e.weder@drukwerkdeal.nl\',\'Eric\',\'Weder\',\'803A85D2748104\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'erwin.tenhoornboer\',\'erwin.tenhoornboer@people-print.com\',\'Erwin\',\'ten Hoorn Boer\',\'803A85D2444E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'esperanza.claasen\',\'esperanza.claasen@drukwerkdeal.nl\',\'Esperanza\',\'Claasen\',\'803A85D2624F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'esther.tuinenga\',\'esther.tuinenga@drukwerkdeal.nl\',\'Esther\',\'Tuinenga\',\'803A85D2802504\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'etienne.torcque\',\'e.torcque@drukwerkdeal.nl\',\'Etienne\',\'Torcque\',\'803A85D26F9604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'evertjan.schouten\',\'e.schouten@drukwerkdeal.nl\',\'Evert Jan\',\'Schouten\',\'803665FA296704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'femke.bosma\',\'femke.bosma@drukwerkdeal.nl\',\'Femke\',\'Bosma\',\'803A85D28C3904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'frederique.niederer\',\'frederique.niederer@drukwerkdeal.nl\',\'Frederique\',\'Niederer\',\'803A85D2606704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'gijs.koning\',\'g.koning@drukwerkdeal.nl\',\'Gijs\',\'Koning\',\'803A85D2711B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'haniek.awanes\',\'haniek.awanes@drukwerkdeal.nl\',\'Haniek\',\'Awanes\',\'803A85D27B4004\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'hanneke.delange\',\'h.delange@drukwerkdeal.nl\',\'Hanneke\',\'de Lange\',\'803A85D2666104\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'harmen.bosveld\',\'harmen.bosveld@drukwerkdeal.nl\',\'Harmen\',\'Bosveld\',\'803A85D2643304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'henny.beumer\',\'h.beumer@drukwerkdeal.nl\',\'Henny\',\'Beumer\',\'803A85D27F7E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'inge.hilberink\',\'inge.hilberink@drukwerkdeal.nl\',\'Inge\',\'Hilberink\',\'803A85D2648C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'inge.smits\',\'i.smits@drukwerkdeal.nl\',\'Inge\',\'Smits\',\'803A85D2599304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ingrid.jansen\',\'i.jansen@drukwerkdeal.nl\',\'Ingrid\',\'Jansen\',\'803A85D2524A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'iris.hoek\',\'iris.hoek@drukwerkdeal.nl\',\'Iris\',\'Hoek\',\'803A85D27E4704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'iwan.vlierman\',\'iwan.vlierman@drukwerkdeal.nl\',\'Iwan\',\'Vlierman\',\'PUK9aUqzgwtOv0\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'janine.schepers\',\'janine.schepers@drukwerkdeal.nl\',\'Janine\',\'Schepers\',\'803A85D27F6E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'janjaap.holdijk\',\'janjaap.holdijk@drukwerkdeal.nl\',\'Jan Jaap\',\'Holdijk\',\'803A85D2410D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jasmijn.hemersma\',\'jasmijn.hemersma@drukwerkdeal.nl\',\'Jasmijn\',\'Hemersma\',\'803A85D25F2604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jasper.draaijer\',\'j.draaijer@drukwerkdeal.nl\',\'Jasper\',\'Draaijer\',\'803A85D2502D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jasper.hegeman\',\'jasper.hegeman@drukwerkdeal.nl\',\'Jasper\',\'Hegeman\',\'803A85D26E3204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jeroen.kouijzer\',\'jeroen.kouijzer@drukwerkdeal.nl\',\'Jeroen\',\'Kouijzer\',\'803665FA296F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jeroen.severein\',\'jeroen.severein@people-print.com\',\'Jeroen\',\'Severein\',\'803A85D26C9404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jesper.vanderwal\',\'\',\'Jesper\',\'van der Wal\',\'803A85D2664E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jessica.groenhout\',\'j.groenhout-kolkman@drukwerkdeal.nl\',\'Jessica\',\'Groenhout-Kolkman\',\'803A85D2736904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jessica.jansen\',\'jessica.jansen@drukwerkdeal.nl\',\'Jessica\',\'Jansen\',\'803A85D24F0D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'joost.vanbockel\',\'joost.vanbockel@people-print.com\',\'Joost\',\'van Bockel\',\'803A85D2887204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'joris.kroes\',\'joris.kroes@drukwerkdeal.nl\',\'Joris\',\'Kroes\',\'803A85D27A1804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jose.vandenbroek\',\'j.vandenbroek@drukwerkdeal.nl\',\'JosÃ©\',\'van den Broek\',\'803A85D2518F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'judith.bovenlander\',\'judith.bovelander@drukwerkdeal.nl\',\'Judith\',\'Bovelander\',\'803666124A1804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jurgen.siero\',\'jurgen.siero@drukwerkdeal.nl\',\'Jurgen\',\'Siero\',\'803A85D25D1F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'koen.roumen\',\'koen.roumen@drukwerkdeal.nl\',\'Koen\',\'Roumen\',\'803A85D2473E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'koen.schouwenaars\',\'koen.schouwenaars@drukwerkdeal.nl\',\'Koen\',\'Schouwenaars\',\'803A85D2762304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'kyra.denouden\',\'k.denouden@drukwerkdeal.nl\',\'Kyra\',\'den Ouden\',\'803A85D2812804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'l.vaneijkelbrouwer\',\'l.vaneijkelbrouwer@drukwerkdeal.nl\',\'Lyanne\',\'van Eijkel Brouwer\',\'80366612492E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'leona.schaafhofstede\',\'leona.schaaf@drukwerkdeal.nl\',\'Leona\',\'Schaaf Hofstede\',\'803A85D2521704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lex.utama\',\'lex.utama@drukwerkdeal.nl\',\'Lex\',\'Utama\',\'803A85D25F1404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'liesbeth.hoogland\',\'liesbeth.hoogland@drukwerkdeal.nl\',\'Liesbeth\',\'Hoogland\',\'803A85D24E0E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'linda.koenrades\',\'l.koenrades@drukwerkdeal.nl\',\'Linda\',\'Koenrades\',\'803A85D2489604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'linda.schulenburg\',\'l.schulenburg@drukwerkdeal.nl\',\'Linda\',\'Schulenburg\',\'80366612458404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'linda.telling\',\'l.telling@drukwerkdeal.nl\',\'Linda\',\'Telling\',\'80366612492604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'linda.vanderstruik\',\'l.vanderstruik@drukwerkdeal.nl\',\'Linda\',\'van der Struik\',\'803A85D2878204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lindsay.egberink\',\'lindsay.egberink@drukwerkdeal.nl\',\'Lindsay\',\'Egberink\',\'803A85D2478604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lonieke.schepers\',\'lonieke.schepers@drukwerkdeal.nl\',\'Lonieke\',\'Schepers\',\'803665FA2B1904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lonneke.verwoolde\',\'lonneke.verwoolde@drukwerkdeal.nl\',\'Lonneke\',\'Verwoolde\',\'803A85D2711104\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lourens.scholing\',\'lourens.scholing@drukwerkdeal.nl\',\'Lourens\',\'Scholing\',\'803A85D2421504\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lucas.baarspul\',\'lucas.baarspul@drukwerkdeal.nl\',\'Lucas\',\'Baarspul\',\'803A85D27F2304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'m.vangeerenstein\',\'m.vangeerenstein@drukwerkdeal.nl\',\'Maarten\',\'van Geerenstein\',\'803665FA2B3F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'marco.lepoole\',\'m.lepoole@drukwerkdeal.nl\',\'Marco\',\'Le Poole\',\'803A85D2632704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'marian.gotz\',\'m.gotz@drukwerkdeal.nl\',\'Marian\',\'Gotz\',\'803A85D26F7F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mariano.garcia\',\'mariano.garcia@drukwerkdeal.nl\',\'Mariano\',\'Garcia\',\'803A85D2614D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mariska.mediati\',\'mariska.mediati@drukwerkdeal.nl\',\'Mariska\',\'Mediati\',\'803A85D2667604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'marissa.spoolder\',\'m.spoolder@drukwerkdeal.nl\',\'Marissa\',\'Spoolder\',\'803A85D25C8804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mark.bolink\',\'mark.bolink@drukwerkdeal.nl\',\'Mark\',\'Bolink\',\'803A85D2570F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'marlon.moes\',\'m.moes@drukwerkdeal.nl\',\'Marlon\',\'Moes\',\'803A85D2531C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martien.bekema\',\'martien.bekema@drukwerkdeal.nl\',\'Martien\',\'Bekema\',\'80366612440604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martijn.devries\',\'martijn.devries@drukwerkdeal.nl\',\'Martijn\',\'de Vries\',\'803A85D2676604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martijn.jansen\',\'martijn.jansen@drukwerkdeal.nl\',\'Martijn\',\'Jansen\',\'803A85D2612004\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martijnscan\',\'m.devries@drukwerkdeal.nl\',\'Martijn\',\'de Vries (Scan)\',\'803A85D25E7F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martyn.graat\',\'martyn.graat@drukwerkdeal.nl\',\'Martyn\',\'Graat\',\'80366612497604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mascha.eilers\',\'m.eilers@drukwerkdeal.nl\',\'Mascha\',\'Eilers\',\'803A85D2597904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'menno.seegers\',\'m.seegers@drukwerkdeal.nl\',\'Menno\',\'Seegers\',\'803A85D24C4604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'michael.willems\',\'michael.willems@drukwerkdeal.nl\',\'Michael\',\'Willems\',\'803A85D25F7504\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'michel.brans\',\'michel.brans@drukwerkdeal.nl\',\'Michel\',\'Brans\',\'803A85D2787A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'michelle.pouwels\',\'michelle.pouwels@drukwerkdeal.nl\',\'Michelle\',\'Pouwels\',\'803A85D2569304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mirko.vrielink\',\'mirko.vrielink@people-print.com\',\'Mirko\',\'Vrielink\',\'803A85D2874D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mitch.brinkman\',\'mitch.brinkman@drukwerkdeal.nl\',\'Mitch\',\'Brinkman\',\'803A85D24E1004\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mohammed.mohammed\',\'mohammed.mohammed@drukwerkdeal.nl\',\'Mohammed\',\'Mohammed\',\'803A85D2741104\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'monique.verheij\',\'m.verheij@drukwerkdeal.nl\',\'Monique\',\'Verheij\',\'80366612443B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'nathalie.lange\',\'nathalie.lange@drukwerkdeal.nl\',\'Nathalie\',\'Lange\',\'803A85D2677C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'nicholas.zaaijer\',\'nicholas.zaaijer@drukwerkdeal.nl\',\'Nicholas\',\'Zaaijer\',\'803A85D2641904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'odwin.rensen\',\'odwin.rensen@drukwerkdeal.nl\',\'Odwin\',\'Rensen\',\'803A85D2726704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'patrick.brinkman\',\'p.brinkman@drukwerkdeal.nl\',\'Patrick\',\'Brinkman\',\'803A85D2744304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'patrick.brinkman\',\'p.brinkman@drukwerkdeal.nl\',\'Patrick\',\'Brinkman\',\'PUK20uNSXJUlJg\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'paul.deweerd\',\'paul.deweerd@drukwerkdeal.nl\',\'Paul\',\'de Weerd\',\'803A85D2607F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'paulien.schippers\',\'p.schippers@drukwerkdeal.nl\',\'Paulien\',\'Schippers\',\'803A85D2439C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'peter.tuk\',\'p.tuk@drukwerkdeal.nl\',\'Peter\',\'Tuk\',\'803A85D2570804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'peter.vuyk\',\'p.vuyk@drukwerkdeal.nl\',\'Peter\',\'Vuyk\',\'803A85D27E4004\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'pleuni.lucassen\',\'pleuni.lucassen@drukwerkdeal.nl\',\'Pleuni\',\'Lucassen\',\'80366612405204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'remco.aarnink\',\'remco.aarnink@drukwerkdeal.nl\',\'Remco\',\'Aarnink\',\'803A85D27A3F04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'renate.vanhilst\',\'renate.vanhilst@drukwerkdeal.nl\',\'Renate\',\'van Hilst\',\'803A85D24A0904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'rene.lensink\',\'rene.lensink@people-print.com\',\'Rene\',\'Lensink\',\'803A85D2797304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'reno.dejong\',\'reno.dejong@drukwerkdeal.nl\',\'Reno\',\'de Jong\',\'803A85D2755C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'rens.bluemink\',\'r.bluemink@drukwerkdeal.nl\',\'Rens\',\'Bluemink\',\'803A85D25A5604\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'richard.winklaar\',\'r.winklaar@drukwerkdeal.nl\',\'Richard\',\'Winklaar\',\'803A85D2643704\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'richard.wolterink\',\'richard.wolterink@drukwerkdeal.nl\',\'Richard\',\'Wolterink\',\'803A85D2682304\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'roald.boone\',\'r.boone@drukwerkdeal.nl\',\'Roald\',\'Boone\',\'803A85D2603204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'roy.mulder\',\'roy.mulder@drukwerkdeal.nl\',\'Roy\',\'Mulder\',\'803A85D2507B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'roy.schonewille\',\'roy.schonewille@drukwerkdeal.nl\',\'Roy\',\'Schonewille\',\'803A85D24E3904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sander.scheer\',\'sander.scheer@drukwerkdeal.nl\',\'Sander\',\'Scheer\',\'803A85D27B5904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sanne.meerbeek\',\'s.meerbeek@drukwerkdeal.nl\',\'Sanne\',\'Meerbeek\',\'803A85D2680D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sanne.nieuwland\',\'sanne.nieuwland@drukwerkdeal.nl\',\'Sanne\',\'Nieuwland\',\'803A85D26E0E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'shirzad.saidi\',\'shirzad.saidi@drukwerkdeal.nl\',\'Shirzad\',\'Saidi\',\'803A85D24B1204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'simon.guchelaar\',\'s.guchelaar@drukwerkdeal.nl\',\'Simon\',\'Guchelaar\',\'803A85D2777D04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'simone.loef\',\'s.loef@drukwerkdeal.nl\',\'Simone\',\'Loef\',\'803A85D2489004\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sjors.brugge\',\'s.brugge@drukwerkdeal.nl\',\'Sjors\',\'Brugge\',\'803A85D2825204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sophie.jonker\',\'s.jonker@drukwerkdeal.nl\',\'Sophie\',\'Jonker\',\'803A85D27E2E04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'stijn.berends\',\'stijn.berends@drukwerkdeal.nl\',\'Stijn\',\'Berends\',\'803A85D2503904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'susan.duivenvoorde\',\'s.duivenvoorde@drukwerkdeal.nl\',\'Susan\',\'Duivenvoorde\',\'PUKVfXAFRQ1ZUE\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'susanne.langenakker\',\'s.langenakker@drukwerkdeal.nl\',\'Susanne\',\'Langenakker\',\'803A85D2664404\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sylvia.vroklage\',\'sylvia.vroklage@drukwerkdeal.nl\',\'Sylvia\',\'Vroklage\',\'803A85D2553C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tanja.rutjes\',\'tanja.rutjes@drukwerkdeal.nl\',\'Tanja\',\'Rutjes\',\'803A85D2577904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tessa.lahuis\',\'tessa.lahuis@drukwerkdeal.nl\',\'Tessa\',\'Lahuis\',\'803A85D27D6804\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tim.huijzers\',\'tim.huijzers@drukwerkdeal.nl\',\'Tim\',\'Huijzers\',\'80366612406A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tim.schierbeek\',\'tim.schierbeek@drukwerkdeal.nl\',\'Tim\',\'Schierbeek\',\'803A85D2655904\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tom.nijboer\',\'tom.nijboer@drukwerkdeal.nl\',\'Tom\',\'Nijboer\',\'803A85D26F8B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ton.keunen\',\'ton.keunen@drukwerkdeal.nl\',\'Ton\',\'Keunen\',\'803A85D25D6C04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tony.demeijer\',\'tony.demeijer@drukwerkdeal.nl\',\'Tony\',\'de Meijer\',\'803A85D24E1B04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'winny.paaymans\',\'winny.paaymans@drukwerkdeal.nl\',\'Winny\',\'Paaymans\',\'803A85D2459204\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'wouter.bax\',\'w.bax@drukwerkdeal.nl\',\'Wouter\',\'Bax\',\'803A85D2877A04\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'andry.cucca\',\'andry.cucca@printdeal.be\',\'Andry\',\'Cucca\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'anne.terweele\',\'a.terweele@drukwerkdeal.nl\',\'Anne\',\'ter Weele\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'annelies.deweerd\',\'annelies.deweerd@drukwerkdeal.nl\',\'Annelies\',\'de Weerd\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'bastiaan.wesselius\',\'b.wesselius@drukwerkdeal.nl\',\'Bastiaan\',\'Wesselius\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'cor.haanstra\',\'cor.haanstra@drukwerkdeal.nl\',\'Cor\',\'Haanstra\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'daniel.vanderberg\',\'daniel.vanderberg@drukwerkdeal.nl\',\'Daniel\',\'van der Berg\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'daphne.zomermaand\',\'d.zomermaand@drukwerkdeal.nl\',\'Daphne\',\'Zomermaand\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dario.valiant\',\'\',\'Dario\',\'Valiant Casilli\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'dea.goulmy\',\'d.goulmy@drukwerkdeal.nl\',\'Dea\',\'Goulmy\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'edi.siegers\',\'ei.siegers@drukwerkdeal.nl\',\'Edi\',\'Siegers\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ellen.smeitink\',\'e.smeitink@drukwerkdeal.nl\',\'Ellen\',\'Smeitink\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'emma.houben\',\'e.houben@printdeal.be\',\'Emma\',\'Houben\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'erwin.everts\',\'erwin.everts@drukwerkdeal.nl\',\'Erwin\',\'Everts\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'eva.stern\',\'e.stern@drukwerkdeal.nl\',\'Eva\',\'Stern\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'fred.vandendriessche\',\'fred.vandendriessche@drukwerkdeal.nl\',\'Fred\',\'van den Driessche\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'gertjan.groothedde\',\'g.groothedde@drukwerkdeal.nl\',\'Gertjan\',\'Groothedde\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'gilian.schreuders\',\'gilian.schreuders@drukwerkdeal.nl\',\'Gilian\',\'Schreuders\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'haman.dan\',\'haman.dan@drukwerkdeal.nl\',\'Haman\',\'Dan\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'hans.vloedgraven\',\'h.vloedgraven@drukwerkdeal.nl\',\'Hans\',\'Vloedgraven\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'herman.eikelboom\',\'herman.eikelboom@drukwerkdeal.nl\',\'Herman\',\'Eikelboom\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'ilve.schoeters\',\'ilve.schoeters@printdeal.be\',\'Ilve\',\'Schoeters\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'janao.voortman\',\'j.voortman@drukwerkdeal.nl\',\'Janao\',\'Voortman\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jasper.vancalster\',\'jasper.vancalster@printdeal.be\',\'Jasper\',\'van Calster\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jeffry.lutfi\',\'j.lutfi@drukwerkdeal.nl\',\'Jeffry\',\'Lutfi\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jelle.smeets\',\'jelle.smeets@drukwerkdeal.nl\',\'Jelle\',\'Smeets\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jelle.weinreder\',\'j.weinreder@drukwerkdeal.nl\',\'Jelle\',\'Weinreder\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jeroen.vanklaveren\',\'jeroen.vanklaveren@drukwerkdeal.nl\',\'Jeroen\',\'van Klaveren\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jonatan.elgersma\',\'jonatan.elgersma@drukwerkdeal.nl\',\'Jonatan\',\'Elgersma\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'jos.dejong\',\'jos.dejong@drukwerkdeal.nl\',\'Jos\',\'de Jong\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'karlijn.vanamerongen\',\'karlijn.vanamerongen@drukwerkdeal.nl\',\'Karlijn\',\'van Amerongen\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'kathy.vanloon\',\'kathy.vanloon@printdeal.be\',\'Kathy\',\'van Loon\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'kees.arends\',\'kees.arends@drukwerkdeal.nl\',\'Kees\',\'Arends\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lina.janssen\',\'l.janssen@printdeal.be\',\'Lina\',\'Janssen\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'lisa.hurkens\',\'lisa.hurkens@drukwerkdeal.nl\',\'Lisa\',\'Hurkens\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'maaike.corporaal\',\'m.corporaal@drukwerkdeal.nl\',\'Maaike\',\'Corporaal\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mark.vanoort\',\'mark.vanoort@people-print.com\',\'Mark\',\'van Oort\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'mark.veerman\',\'mark.veerman@drukwerkdeal.nl\',\'Mark\',\'Veerman\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'marlies.cocquyt\',\'m.cocquyt@printdeal.be\',\'Marlies\',\'Cocquyt\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martijn.maas\',\'m.maas@drukwerkdeal.nl\',\'Martijn\',\'Maas\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martijn.vandepoel\',\'martijn.vandepoel@drukwerkdeal.nl\',\'Martijn\',\'van de Poel\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'martin.schimmel\',\'martin.schimmel@people-print.com\',\'Martin\',\'Schimmel\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'michael.willems\',\'michael.willems@people-print.com\',\'Michael\',\'Willems\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'pim.vangurp\',\'pim.vangurp@drukwerkdeal.nl\',\'Pim\',\'van Gurp\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'riemer.walinga\',\'r.walinga@drukwerkdeal.nl\',\'Riemer\',\'Walinga\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'rob.visser\',\'rob.visser@drukwerkdeal.nl\',\'Rob\',\'Visser\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'robin.bosch\',\'r.bosch@drukwerkdeal.nl\',\'Robin\',\'Bosch\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'roel.thielens\',\'roel.thielens@printdeal.be\',\'Roel\',\'Thielens\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'roy.selbach\',\'roy.selbach@printdeal.be\',\'Roy\',\'Selbach\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sergejs.mironovs\',\'\',\'Sergejs\',\'Mironovs\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'sura.hulleman\',\'sura.hulleman@drukwerkdeal.nl\',\'Sura\',\'Hulleman\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tamara.tielen\',\'tamara.tielen@drukwerkdeal.nl\',\'Tamara\',\'Tielen\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'testdevelopment\',\'\',\'Test\',\'Development\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'thomas.croonen\',\'thomas.croonen@people-print.com\',\'Thomas\',\'Croonen\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'tino.cucca\',\'tino.cucca@printdeal.be\',\'Tino\',\'Cucca\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'vincent.bloos\',\'v.bloos@drukwerkdeal.nl\',\'Vincent\',\'Bloos\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'wilbert.besselink\',\'wilbert.besselink@drukwerkdeal.nl\',\'Wilbert\',\'Besselink\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'wout.vangils\',\'wout.vangils@printdeal.be\',\'Wout\',\'van Gils\',\'\');');
        $this->addSql('INSERT INTO safe_q_user(`username`, `email`, `first_name`, `last_name`, `rfid`) VALUES(\'wouter.vogelzang\',\'wouter.vogelzang@drukwerkdeal.nl\',\'Wouter\',\'Vogelzang\',\'\');');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE safe_q_user');
    }
}
