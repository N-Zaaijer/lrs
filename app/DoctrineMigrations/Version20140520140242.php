<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140520140242 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO `role` (`id`, `name`, `created_by`, `creation_time`, `modified_by`, `modification_time`)
            VALUES
                (1, 'ROLE_ADMIN', NULL, '2014-04-24 14:39:17', NULL, NULL),
                (2, 'ROLE_USER', NULL, '2014-04-24 14:39:25', NULL, NULL)");

    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `role`");
    }
}
