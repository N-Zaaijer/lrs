<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20151218152416 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE lunch_registration_log (id INT UNSIGNED AUTO_INCREMENT NOT NULL COMMENT \'Unique identifier of lunch entry\', safe_q_user_id INT UNSIGNED NOT NULL COMMENT \'Unique identifier of SafeQ User\', lunch_date DATE NOT NULL COMMENT \'Date RFID was last scanned for lunch\', UNIQUE INDEX UniqueEntry (safe_q_user_id, lunch_date), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'Table with users that are checked in for lunch\' ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE lunch_registration_log');
    }
}
