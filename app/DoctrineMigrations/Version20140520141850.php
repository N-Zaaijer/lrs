<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140520141850 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO `user_has_role` (`user_id`, `role_id`) VALUES
            (1, 1),	(1, 2), (2, 2)");
    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `user_has_role`");
    }
}
