<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140520134144 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("INSERT INTO `user` (`id`, `username`, `password`, `salt`, `secret`, `email`, `created_by`, `creation_time`, `modified_by`, `modification_time`)
            VALUES
            (1, 'admin', 'aaa0324a8e60bc384ebbeb643827598209e9bc29', 'user3-sha-salt', '8bb5780ca06dfaa4ac1bdd2af6b00a3520f6e35d', 'email@email.test', NULL, NOW(), NULL, NULL),
            (2, 'reportal', 'dd85a99dc7e24d576df3026aa5e7e0dfdb95dc05', 'reportal-sha-salt', '314e047b72998e636adafcd57fa34ad006adffb0', 'reportal@void.email', 1, NOW(), NULL, NULL)"
        );
    }

    public function down(Schema $schema)
    {
        $this->addSql("DELETE FROM `user`");
    }
}
