<?php

namespace DWD\StatusCheckBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * Class ServerStatusController
 * @package DWD\StatusCheckBundle\Controller
 */
class ServerStatusController extends Controller
{
    /**
     * Check Service Availability.
     * Response example: {"Mem-Usage":70,"CPU-Usage":0,"Disk-Space":6063024,"Used-Disk-Space-Percents":26}
     * @ApiDoc(
     *      statusCodes={
     *         200="Returned when successful",
     *     }
     * )
     * @return Response
     */
    public function statusCheckAction()
    {

        $systemStatus = array(
            'Mem-Usage' => $this->getMemoryUsage(),
            'CPU-Usage' => $this->getCpuUsage(),
            'Disk-Space' => $this->getFreeDiskspace(),
            'Used-Disk-Space-Percents' => $this->getDiskUsageInPercent()
        );
        return new Response(json_encode($systemStatus), Response::HTTP_OK);
    }

    /**
     * @return int, ammount of free diskspace in KB
     */
    private function getFreeDiskspace()
    {
        $bytes = disk_free_space('/');
        $kb = $bytes / 1024;
        return (int)$kb;
    }

    /**
     * @return int, CPU loading in percent
     */
    private function getCpuUsage()
    {
        if (function_exists('sys_getloadavg')) {
            $load = sys_getloadavg(); //returns array with last 1,5,15 mins data
            $load = $load[0]; // takes avg load for last minute
        } else {
            $load = -1;
        }

        return $load;
    }

    /**
     * @return int
     */
    private function getDiskUsageInPercent()
    {
        $freeSpace = disk_free_space('/');
        $totalDiskSpace = disk_total_space('/');
        $usedDiskSpace = $totalDiskSpace - $freeSpace;
        $usedDiskSpaceInPercent = $usedDiskSpace / $totalDiskSpace * 100;
        return (int)$usedDiskSpaceInPercent;
    }

    /**
     * @return int, ammount of used memory in percent
     */
    private function getMemoryUsage()
    {
        $infoString = @file_get_contents("/proc/meminfo");
        if (!$infoString) {
            return -1;
        }

        $data = explode("\n", $infoString);
        $meminfo = array();
        foreach ($data as $line) {
            if (strpos($line, ':') !== false) {
                list($key, $val) = explode(":", $line);
                $val = str_replace('kB', '', $val);
                $meminfo[$key] = trim($val);
            }
        }
        $usedMemory = (int)$meminfo['MemTotal'] - (int)$meminfo['MemFree'];
        $usedMemoryInPercent = $usedMemory / (int)$meminfo['MemTotal'] * 100;


        return (int)$usedMemoryInPercent;
    }
}
