<?php

namespace DWD\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class HomePageController
 * @package DWD\AdminBundle\Controller
 */
class HomePageController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render(
            'DWDAdminBundle:HomePage:index.html.twig'
        );
    }
}
