<?php

namespace DWD\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PassLostController
 * @package DWD\AdminBundle\Controller
 */
class PassLostController extends Controller
{
    const PASS_DEFAULT_LENGTH = 10;

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $translator = $this->get('translator');
        $error = '';
        $sendSuccessful = false;
        $email = $request->get('_email');
        if ($email) {
            $em = $this->getDoctrine()->getManager();

            $entity = $em->getRepository('DWDAdminBundle:User')->findOneByEmail($email);
            if (!$entity) {
                $error = $translator->trans('user.passlost.error.userNotExists');
            } else {
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($entity);
                $randPass = $this->randPassword(self::PASS_DEFAULT_LENGTH);
                $password = $encoder->encodePassword($randPass, $entity->getSalt());
                $entity->setPassword($password);
                $url = $this->container->get('router')->getContext()->getHost() . $this->generateUrl('login');

                $message = $translator->trans(
                    'user.passlost.emailMessageBody',
                    array(
                        '%username%' => $entity->getUsername(),
                        '%password%' => $randPass,
                        '%url%' => $url,
                    )
                );
                $subject = $translator->trans('user.passlost.emailMessageSubject');

                $headers = array();
                $headers[] = "MIME-Version: 1.0";
                $headers[] = "Content-type: text/html; charset=iso-8859-1";
                $headers[] = "From: Wordcloud <donotreply@wordcloud.com>";
                $headers[] = "Subject: {$subject}";
                $headers[] = "X-Mailer: PHP/" . phpversion();


                $status = mail($email, $subject, $message, implode("\r\n", $headers));
                if ($status) {
                    $em->persist($entity);
                    $em->flush();
                    $sendSuccessful = true;
                } else {
                    $error = $translator->trans('user.passlost.error.emailCantBeSent');
                }
            }
        }

        return $this->render(
            'DWDAdminBundle:PassLost:index.html.twig',
            array(
                'success' => $sendSuccessful,
                'error' => $error,
            )
        );

    }

    /**
     * Random generating of new password
     * @param $length
     * @return string
     */
    private function randPassword($length)
    {

        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);

    }
}
