<?php

namespace DWD\AdminBundle\Controller;

use DWD\AdminBundle\Entity\LunchRegistrationLog;
use DWD\AdminBundle\Entity\SafeQUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;

/**
 * Class HomePageController
 * @package DWD\AdminBundle\Controller
 */
class ApiController extends Controller
{

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $id = $request->get("id");

        $id = array_reverse(explode(',', str_replace(' ', '', $id)));

        $rfid = '';
        foreach ($id as $value){
            $rfid .= str_pad(substr($value, 2), 2, '0', STR_PAD_LEFT);
        };

        $user = $this->getDoctrine()
            ->getRepository('DWDAdminBundle:SafeQUser')
            ->findOneByRfid($rfid);
        if(!$user instanceof SafeQUser) {
            return new JsonResponse(array("idfound" => false, "name" => false, "id" => $request->get("id")));
        }

        $user->addLunch(new LunchRegistrationLog($user));

        $em = $this->getDoctrine()->getManager();

        try {
            $em->persist($user);
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            // do nothing
        } catch (\Exception $e) {
            return new JsonResponse(
                array(
                    "idfound" => true,
                    "name" => $e->getMessage(),
                    "id" => $request->get("id")
                )
            );
        }

        return new JsonResponse(
            array(
                "idfound" => true,
                "name" => sprintf('%s %s', $user->getFirstName(), $user->getLastName()),
                "id" => $request->get("id")
            )
        );
    }
}
