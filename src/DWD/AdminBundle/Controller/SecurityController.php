<?php
/**
 * Created by PhpStorm.
 * User: d.kiprushev
 * Date: 31.03.14
 * Time: 11:55
 */
namespace DWD\AdminBundle\Controller;

use DWD\AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Security;

/**
 * Class SecurityController
 * @package DWD\AdminBundle\Controller
 */
class SecurityController extends Controller
{
    /**
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function loginAction()
    {
        if ($this->getUser() instanceof User) {
            return new RedirectResponse(
                $this->generateUrl('dwd_admin_homepage')
            );
        }

        $request = $this->get('request_stack')->getCurrentRequest();
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
        }

        return $this->render(
            'DWDAdminBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $session->get(Security::LAST_USERNAME),
                'error' => $error,
            )
        );
    }
}
