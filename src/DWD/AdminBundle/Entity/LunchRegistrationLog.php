<?php
namespace DWD\AdminBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\DateType;

/**
 * LunchRegistrationLog
 *
 * @ORM\Entity
 * @ORM\Table(
 *  name="lunch_registration_log",
 *  options={"comment":"Table with users that are checked in for lunch"},
 *  uniqueConstraints={@ORM\UniqueConstraint(name="UniqueEntry", columns={"safe_q_user_id", "lunch_date"})})
 * @UniqueEntity(fields={"SafeQUser", "lunchDate"}, message="user already registered")
 * @ORM\HasLifecycleCallbacks
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class LunchRegistrationLog {

    /**
     * Lunch entry id
     *
     * @var integer
     * @ORM\Column(
     *  name="id",
     *  type="integer",
     *  options={
     *      "unsigned":"true",
     *      "comment":"Unique identifier of lunch entry"
     *  })
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SerializedName("id")
     */
    private $id;

    /**
     * SafeQ User id
     *
     * @var integer
     * @ORM\Column(
     *  name="safe_q_user_id",
     *  type="integer",
     *  options={
     *      "unsigned":"true",
     *      "comment":"Unique identifier of SafeQ User"
     *  })
     * @ORM\ManyToOne(targetEntity="SafeQUser", inversedBy="lunches", )
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SerializedName("safeQUserId")
     */
    private $safeQUserId;

    /**
     * Lunch date
     *
     * @var \DateTime
     *
     * @ORM\Column(
     *  name="lunch_date",
     *  type="date", options={
     *      "comment":"Date RFID was last scanned for lunch"
     *  })
     * @SerializedName("lunchDate")
     */
    private $lunchDate;

    function __construct(SafeQUser $safeQUser)
    {
        $this->safeQUserId = $safeQUser->getId();
    }


    /**
     * Get safeQUserId
     *
     * @return integer
     */
    public function getSafeQUserId()
    {
        return $this->safeQUserId;
    }

    /**
     * Set lunchDate
     *
     * @param \DateTime $lunchDate
     *
     * @return LunchRegistrationLog
     */
    public function setLunchDate($lunchDate)
    {
        $this->lunchDate = $lunchDate;

        return $this;
    }

    /**
     * Get lunchDate
     *
     * @return \DateTime
     */
    public function getLunchDate()
    {
        return $this->lunchDate;
    }

    /**
     *  @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->lunchDate = new \DateTime();
    }
}
