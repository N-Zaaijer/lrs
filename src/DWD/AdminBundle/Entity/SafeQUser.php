<?php
namespace DWD\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Type;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SafeQUser
 *
 * @ORM\Entity
 * @ORM\Table(name="safe_q_user", options={"comment":"Table with SafeQ users"})
 * @ORM\HasLifecycleCallbacks
 *
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class SafeQUser {

    /**
     * SafeQ User id
     *
     * @var integer
     * @ORM\Column(
     *  name="id",
     *  type="integer",
     *  options={
     *      "unsigned":"true",
     *      "comment":"Unique identifier of SafeQ User"
     *  })
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SerializedName("id")
     */
    private $id;

    /**
     * SafeQ Username
     *
     * @var string
     *
     * @ORM\Column(
     *  name="username",
     *  type="text",
     *  options={
     *      "comment":"Textual identifier for SafeQ user"
     *  })
     * @SerializedName("username")
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * SafeQ user email address
     *
     * @var string
     *
     * @ORM\Column(
     *  name="email",
     *  type="text",
     *  options={
     *      "comment":"Email address of SafeQ user"
     *  })
     * @SerializedName("email")
     */
    private $email;

    /**
     * First name of SafeQ user 
     *
     * @var string
     *
     * @ORM\Column(
     *  name="first_name",
     *  type="text",
     *  options={
     *      "comment":"First name of SafeQ user"
     *  })
     * @SerializedName("firstName")
     */
    private $firstName;

    /**
     * Last name of SafeQ user
     *
     * @var string
     *
     * @ORM\Column(
     *  name="last_name",
     *  type="text",
     *  options={
     *      "comment":"last name of SafeQ user"
     *  })
     * @SerializedName("lastName")
     */
    private $lastName;

    /**
     * RFID of SafeQ user
     *
     * @var string
     *
     * @ORM\Column(
     *  name="rfid",
     *  type="text",
     *  options={
     *      "comment":"RFID of SafeQ user"
     *  })
     * @SerializedName("rfid")
     */
    private $rfid;

    /**
     * Last scan date
     *
     * @var \DateTime
     *
     * @ORM\Column(
     *  name="last_scan_date",
     *  type="datetime", options={
     *      "comment":"Date RFID was last scanned"
     *  })
     * @SerializedName("lastScanDate")
     */
    private $lastScanDate;

    /**
     * Lunches registered to user
     *
     * @var LunchRegistrationLog[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="LunchRegistrationLog", mappedBy="safeQUserId", cascade={"persist"})
     **/
    private $lunches;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return SafeQUser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return SafeQUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return SafeQUser
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return SafeQUser
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set rfid
     *
     * @param string $rfid
     *
     * @return SafeQUser
     */
    public function setRfid($rfid)
    {
        $this->rfid = $rfid;

        return $this;
    }

    /**
     * Get rfid
     *
     * @return string
     */
    public function getRfid()
    {
        return $this->rfid;
    }

    /**
     * Set lastScanDate
     *
     * @param \DateTime $lastScanDate
     *
     * @return SafeQUser
     */
    public function setLastScanDate($lastScanDate)
    {
        $this->lastScanDate = $lastScanDate;

        return $this;
    }

    /**
     * Get lastScanDate
     *
     * @return \DateTime
     */
    public function getLastScanDate()
    {
        return $this->lastScanDate;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lunches = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add lunch
     *
     * @param \DWD\AdminBundle\Entity\LunchRegistrationLog $lunch
     *
     * @return SafeQUser
     */
    public function addLunch(\DWD\AdminBundle\Entity\LunchRegistrationLog $lunch)
    {
        $this->lunches[] = $lunch;

        return $this;
    }

    /**
     * Remove lunch
     *
     * @param \DWD\AdminBundle\Entity\LunchRegistrationLog $lunch
     */
    public function removeLunch(\DWD\AdminBundle\Entity\LunchRegistrationLog $lunch)
    {
        $this->lunches->removeElement($lunch);
    }

    /**
     * Get lunches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLunches()
    {
        return $this->lunches;
    }

    /**
     *  @ORM\PrePersist
     */
    public function prePersist()
    {
        $this->lastScanDate = new \DateTime("now");
    }
}
