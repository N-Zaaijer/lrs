<?php
namespace DWD\SecurityBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;

/**
 * Class ApiProxyController
 * @package DWD\LanguageServiceAPIBundle\Controller
 */
class ApiProxyController extends FOSRestController
{
    /** Proxy for  Api requests
     * @param Request $request
     * @return Response
     */
    public function apiProxyAction(Request $request)
    {
        $url = $request->get('url');
        $route = $this->get('router')->match($url);
        $controller = $route['_controller'];
        $response = $this->forward($controller, $route);
        return $response;
    }
}
