<?php
namespace DWD\SecurityBundle\Security\WSSE\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class WsseUserToken extends AbstractToken
{
    /**
     * @var string
     */
    public $created;

    /**
     * @var string
     */
    public $digest;

    /**
     * @var string
     */
    public $nonce;

    /**
     * @param array $roles
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        // If the user has roles, consider it authenticated
        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * @return mixed|string
     */
    public function getCredentials()
    {
        return '';
    }
}
