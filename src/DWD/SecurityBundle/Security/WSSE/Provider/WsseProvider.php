<?php
namespace DWD\SecurityBundle\Security\WSSE\Provider;

use DWD\SecurityBundle\Security\WSSE\Token\WsseUserToken;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\NonceExpiredException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * Class WsseProvider
 * @package DWD\SecurityBundle\Security\WSSE\Provider
 */
class WsseProvider implements AuthenticationProviderInterface
{
    /**
     * @var \Symfony\Component\Security\Core\User\UserProviderInterface
     */
    private $userProvider;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var int
     */
    private $lifetime;

    /**
     * @param UserProviderInterface $userProvider
     * @param $cacheDir
     * @param $lifetime
     */
    public function __construct(UserProviderInterface $userProvider, $cacheDir, $lifetime)
    {
        $this->userProvider = $userProvider;
        $this->cacheDir = $cacheDir;
        $this->lifetime = $lifetime;
    }

    /**
     * @param TokenInterface $token
     * @return WsseUserToken|TokenInterface
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationException
     */
    public function authenticate(TokenInterface $token)
    {
        $user = $this->userProvider->loadUserByUsername($token->getUsername());

        if ($user && $this->validateDigest($token->digest, $token->nonce, $token->created, $user->getSecret())) {
            $authenticatedToken = new WsseUserToken($user->getRoles());
            $authenticatedToken->setUser($user);

            return $authenticatedToken;
        }

        throw new AuthenticationException('The WSSE authentication failed.');
    }

    /**
     * This function is specific to Wsse authentication and is only used to help this example
     *
     * For more information specific to the logic here, see
     * https://github.com/symfony/symfony-docs/pull/3134#issuecomment-27699129
     */
    protected function validateDigest($digest, $nonce, $created, $secret)
    {

        // Expire timestamp after 5 minutes

        if (abs(time() - strtotime($created)) > $this->lifetime) {
            return false;
        }

        // Validate that the nonce is *not* used in the last 5 minutes
        // if it has, this could be a replay attack
        $file = $this->cacheDir . DIRECTORY_SEPARATOR . $nonce;
        if (file_exists($file) && (file_get_contents($file) + $this->lifetime) > time()) {
            throw new NonceExpiredException('Previously used nonce detected');
        }
        // If cache directory does not exist we create it
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir, 0777, true);
        }
        file_put_contents($this->cacheDir . DIRECTORY_SEPARATOR . $nonce, time());

        // Validate Secret
        $expected = base64_encode(sha1(base64_decode($nonce) . $created . $secret, true));

        return $digest === $expected;
    }

    /**
     * @param TokenInterface $token
     * @return bool
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof WsseUserToken;
    }
}
