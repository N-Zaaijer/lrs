<?php

namespace DWD\SecurityBundle;

use DWD\SecurityBundle\DependencyInjection\Security\Factory\WsseFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DWDSecurityBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {

        parent::build($container);

        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new WsseFactory());
    }
}
