[opcache]
opcache.memory_consumption = 32
opcache.interned_strings_buffer = 8
opcache.max_accelerated_files = 4000
opcache.validate_timestamps = 1
opcache.revalidate_freq = 0
opcache.fast_shutdown = 1

[other]
realpath_cache_size = 4096k
realpath_cache_ttl = 7200
max_execution_time = 10
memory_limit = {{ php.options.max_upload_size }}
upload_max_filesize = {{ php.options.max_upload_size }}
post_max_size = {{ php.options.max_upload_size }}